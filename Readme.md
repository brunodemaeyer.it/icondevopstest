# Server Configuration

## Update all the packages
```
sudo apt update
sudo apt upgrade
```
## Configure iptables

```
sudo iptables -A INPUT -i lo -j ACCEPT
sudo iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT
sudo iptables -A INPUT -p tcp --sport 1:1023 -j ACCEPT
sudo iptables -A INPUT -p tcp --dport 1:1023 -j ACCEPT
sudo iptables -A OUTPUT -p tcp --sport 1:1023 -j ACCEPT
sudo iptables -A OUTPUT -p tcp --dport 1:1023 -j ACCEPT
sudo iptables -A INPUT -j DROP
```

## Save iptables configuration
```
sudo iptables-save > iptables_fw.conf
```

## Add a new user
```
sudo adduser icondevops
usermod -aG sudo username
```

## Disable root user
```
sudo passwd -l root
```

Note that root user is actually disabled by default on the ubuntu AMI in EC2, instead user "ubuntu" is used by default to log in.

# Docker

## Install Docker

```
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
```
Check the repository, then install
```
apt-cache policy docker-ce
sudo apt install docker-ce
```
Add icondevops to docker group, so no sudo is necessary when running docker commands
```
sudo usermod -aG docker icondevops
```
Switch to the icondevops user
```
su - icondevops
```


## Install docker compose
```
sudo curl -L https://github.com/docker/compose/releases/download/1.26.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

# Docker Compose manifest 

docker-compose.yml will be created in the root folder of the project
We also need an .env file, that contains MYSQL parameters, in the same folder.
A sub folder nginx-conf contains all the NGINX Configuration
For Letsencrypt and certbot, we download options-ssl-nginx.conf also into this folder with the following command:
```
curl -sSLo nginx-conf/options-ssl-nginx.conf https://raw.githubusercontent.com/certbot/certbot/master/certbot-nginx/certbot_nginx/_internal/tls_configs/options-ssl-nginx.conf
```

## Protecting the website using basic authentication

NGINX does not support .htaccess (very well)
See https://www.nginx.com/resources/wiki/start/topics/examples/likeapache-htaccess/

Instead we can use apache2-utils and htpasswd
https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/ and 

Install apache2-utils
```
sudo apt install apache2-utils
```
Create the password file, store it in ~/wordpress/nginx-conf so it will be copied into /etc/nginx/conf.d in the docker container.
This is defined in docker-compose.yml [here](https://gitlab.com/brunodemaeyer.it/icondevopstest/-/blob/master/docker-compose.yml#L45).

```
sudo htpasswd -c ~/wordpress/nginx-conf/.htpasswd icondevops
```
and add it to the NGINX configuration
```
        location / {
                try_files $uri $uri/ /index.php$is_args$args;
                auth_basic "Restricted Content";
                auth_basic_user_file /etc/nginx/conf.d/.htpasswd;
        }
```

Please note that the .htpasswd file is ignored by git, and is not checked into the repository for security reasons.